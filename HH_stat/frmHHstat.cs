﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;
using System.IO;
using System.Diagnostics;

namespace HH_stat
{
    public partial class frmHHstat : Form
    {
        private HHApi _hhApi;//Создаем сеансовую переменную, чтобы она была жива на всем протяжении работы программы - в ее свойствах хранятся параметры результов поиска вакансий (сколько найдено, номер текущей страницы и т.д.)

        public frmHHstat()
        {
            InitializeComponent();

            trwSpecialization.AfterCheck += new TreeViewEventHandler(trwSelectionChanged);
            trwIndustry.AfterCheck += new TreeViewEventHandler(trwSelectionChanged);
            trwRegion.AfterCheck += new TreeViewEventHandler(trwSelectionChanged);

            btn2Begin.Click += new EventHandler(navigButtons);
            btn2End.Click += new EventHandler(navigButtons);
            btnNext.Click += new EventHandler(navigButtons);
            btnPrev.Click += new EventHandler(navigButtons);

            _hhApi = new HHApi();
        }

        private void navigButtons(object sender, System.EventArgs e)//Обработка четырех навигационных кнопок перемещения по страницам результатов поиска
        {
            switch (((Button)sender).Name.ToString())
            {
                case "btn2Begin":
                    SearchVacancies(btnSearch.Tag.ToString(), "0");
                    break;
                case "btn2End":
                    SearchVacancies(btnSearch.Tag.ToString(), (_hhApi.VacanciesPagesTotal - 1).ToString());
                    break;
                case "btnNext":
                    SearchVacancies(btnSearch.Tag.ToString(), (_hhApi.VacanciesCurrentPage + 1).ToString());
                    break;
                case "btnPrev":
                    SearchVacancies(btnSearch.Tag.ToString(), (_hhApi.VacanciesPagesTotal - 1).ToString());
                    break;
                default:
                    break;
            }
        }

        private void trwSelectionChanged(object sender, System.EventArgs e)//Обновление текстового поля с подсказкой по выбранным на текущий момент параметрам поиска вакансий
        {
            string checkedList = "--- Выбранные проф. области:" + Environment.NewLine;

            Dictionary<string, string> dic = _GetCheckedTreeviewNodesNamesAndText(trwSpecialization);
            foreach (var item in dic)
            {
                checkedList += item.Value + Environment.NewLine;
            }

            checkedList += "--- Выбранные отрасли:" + Environment.NewLine;
            dic = _GetCheckedTreeviewNodesNamesAndText(trwIndustry);
            foreach (var item in dic)
            {
                checkedList += item.Value + Environment.NewLine;
            }

            checkedList += "--- Выбранные регионы:" + Environment.NewLine;
            dic = _GetCheckedTreeviewNodesNamesAndText(trwRegion);
            foreach (var item in dic)
            {
                checkedList += item.Value + Environment.NewLine;
            }

            txtSelectedFilters.Text = checkedList;
        }

        private Dictionary<string, string> _GetCheckedTreeviewNodesNamesAndText(TreeView trw)//Получение словаря (пары "Name - Text") отмеченных элементов иерархического списк
        {
            Dictionary<string, string> checkedNodes = new Dictionary<string, string>();
            foreach (TreeNode currentNode in trw.Nodes)
            {
                if (currentNode.Checked)
                {
                    checkedNodes.Add(currentNode.Name, currentNode.Text);
                }
                if (currentNode.Nodes.Count > 0)
                {
                    Dictionary<string, string> checkedChildren = _GetCheckedNodesRecursive(currentNode);
                    foreach (var item in checkedChildren)
                    {
                        checkedNodes.Add(item.Key, item.Value);
                    }
                }
            }

            return checkedNodes;
        }

        private Dictionary<string, string> _GetCheckedNodesRecursive(TreeNode currentNode)
        {
            Dictionary<string, string> checkedNodes = new Dictionary<string, string>();
            foreach (TreeNode child in currentNode.Nodes)
            {
                if (child.Checked)
                {
                    checkedNodes.Add(child.Name, child.Text);
                }
                if (child.Nodes.Count > 0)
                {
                    Dictionary<string, string> checkedChildren = _GetCheckedNodesRecursive(child);
                    foreach (var item in checkedChildren)
                    {
                        checkedNodes.Add(item.Key, item.Value);
                    }
                }
            }

            return checkedNodes;
        }//Рекурсивная вспомогательная функция для обхода многоуровневого дерева Treeview

        private async void btnSpecRefresh_Click(object sender, EventArgs e)
        {
            trwSpecialization.Nodes.Clear();

            List<TreeNode> collNodes = await _hhApi.ConvertDictJsonToTreeNodeListAsync("specializations");
            if (collNodes != null) trwSpecialization.Nodes.AddRange(collNodes.ToArray());
        }

        private async void btnIndustRefresh_Click(object sender, EventArgs e)
        {
            trwIndustry.Nodes.Clear();

            List<TreeNode> collNodes = await _hhApi.ConvertDictJsonToTreeNodeListAsync("industries");
            if (collNodes != null) trwIndustry.Nodes.AddRange(collNodes.ToArray());
        }

        private async void btnRegionRefresh_Click(object sender, EventArgs e)
        {
            trwRegion.Nodes.Clear();

            List<TreeNode> collNodes = await _hhApi.ConvertDictJsonToTreeNodeListAsync("areas");
            if (collNodes != null) trwRegion.Nodes.AddRange(collNodes.ToArray());
        }

        private void btnSearch_Click(object sender, EventArgs e)//Кнопка "Найти"
        {
            Dictionary<string, string> tempDic;
            string searchParams = string.Empty;

            //Формируем строку поиска
            tempDic = _GetCheckedTreeviewNodesNamesAndText(trwRegion);
            if (tempDic.Count > 0) searchParams = _hhApi.CreateSearchString("area", tempDic);

            tempDic = _GetCheckedTreeviewNodesNamesAndText(trwIndustry);
            if (tempDic.Count > 0) searchParams += (searchParams.Length > 0 ? "&" : string.Empty) + _hhApi.CreateSearchString("industry", tempDic);

            tempDic = _GetCheckedTreeviewNodesNamesAndText(trwSpecialization);
            if (tempDic.Count > 0) searchParams += (searchParams.Length > 0 ? "&" : string.Empty) + _hhApi.CreateSearchString("specialization", tempDic);

            btnSearch.Tag = searchParams;//Сохраняем в тег кнопки поиска сформированный запрос, чтобы при навигации по страницам результатов не формировать его заново

            SearchVacancies(searchParams);
        }

        private async void SearchVacancies(string searchParams, string page = "0")//Асинхронная функция поиска (также используется для перемещения по результатам поиска)
        {
            trwSearchResult.Nodes.Clear();
            //Отправляем поисковый запрос, получаем неформатированный результат в виде JSON-структуры
            string rawSearchResult = await _hhApi.Search(searchParams + (page.CompareTo("0") == 0 ? string.Empty : "&page=" + page));
            txtGetReq.Text = rawSearchResult;

            //Парсим JSON-структуру, заполняем иерархический список результатами
            List<TreeNode> vacancies = _hhApi.ConvertVacancySearchResult2Treeview(rawSearchResult);
            groupBox2.Text = string.Format("Найдено вакансий: {0} (страница {1}/{2})", _hhApi.VacanciesFound, _hhApi.VacanciesCurrentPage + 1, _hhApi.VacanciesPagesTotal);
            if (vacancies != null) trwSearchResult.Nodes.AddRange(vacancies.ToArray());
        }

        private void trwSearchResult_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)//Открытие в браузере по умолчанию ссылки, хранящейся в свойстве Name узла, на котором произведен двойной щелчок
        {
            Process.Start(((TreeView)sender).SelectedNode.Name);
        }
    }
}
