﻿namespace HH_stat
{
    partial class frmHHstat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnRegionRefresh = new System.Windows.Forms.Button();
            this.btnIndustRefresh = new System.Windows.Forms.Button();
            this.btnSpecRefresh = new System.Windows.Forms.Button();
            this.trwRegion = new System.Windows.Forms.TreeView();
            this.label3 = new System.Windows.Forms.Label();
            this.trwIndustry = new System.Windows.Forms.TreeView();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.trwSpecialization = new System.Windows.Forms.TreeView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtGetReq = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtSelectedFilters = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.trwSearchResult = new System.Windows.Forms.TreeView();
            this.btn2Begin = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btn2End = new System.Windows.Forms.Button();
            this.tabMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMain.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabMain.Controls.Add(this.tabPage1);
            this.tabMain.Controls.Add(this.tabPage3);
            this.tabMain.Controls.Add(this.tabPage2);
            this.tabMain.Location = new System.Drawing.Point(2, 2);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(866, 704);
            this.tabMain.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnRegionRefresh);
            this.tabPage1.Controls.Add(this.btnIndustRefresh);
            this.tabPage1.Controls.Add(this.btnSpecRefresh);
            this.tabPage1.Controls.Add(this.trwRegion);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.trwIndustry);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.trwSpecialization);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(858, 675);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Справочники";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnRegionRefresh
            // 
            this.btnRegionRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRegionRefresh.Location = new System.Drawing.Point(572, 642);
            this.btnRegionRefresh.Name = "btnRegionRefresh";
            this.btnRegionRefresh.Size = new System.Drawing.Size(279, 28);
            this.btnRegionRefresh.TabIndex = 8;
            this.btnRegionRefresh.Text = "Обновить список регионов";
            this.btnRegionRefresh.UseVisualStyleBackColor = true;
            this.btnRegionRefresh.Click += new System.EventHandler(this.btnRegionRefresh_Click);
            // 
            // btnIndustRefresh
            // 
            this.btnIndustRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnIndustRefresh.Location = new System.Drawing.Point(288, 642);
            this.btnIndustRefresh.Name = "btnIndustRefresh";
            this.btnIndustRefresh.Size = new System.Drawing.Size(279, 28);
            this.btnIndustRefresh.TabIndex = 7;
            this.btnIndustRefresh.Text = "Обновить список отраслей";
            this.btnIndustRefresh.UseVisualStyleBackColor = true;
            this.btnIndustRefresh.Click += new System.EventHandler(this.btnIndustRefresh_Click);
            // 
            // btnSpecRefresh
            // 
            this.btnSpecRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSpecRefresh.Location = new System.Drawing.Point(3, 642);
            this.btnSpecRefresh.Name = "btnSpecRefresh";
            this.btnSpecRefresh.Size = new System.Drawing.Size(279, 28);
            this.btnSpecRefresh.TabIndex = 6;
            this.btnSpecRefresh.Text = "Обновить список проф. областей";
            this.btnSpecRefresh.UseVisualStyleBackColor = true;
            this.btnSpecRefresh.Click += new System.EventHandler(this.btnSpecRefresh_Click);
            // 
            // trwRegion
            // 
            this.trwRegion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.trwRegion.CheckBoxes = true;
            this.trwRegion.Location = new System.Drawing.Point(573, 19);
            this.trwRegion.Name = "trwRegion";
            this.trwRegion.Size = new System.Drawing.Size(279, 617);
            this.trwRegion.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(570, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Регион";
            // 
            // trwIndustry
            // 
            this.trwIndustry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.trwIndustry.CheckBoxes = true;
            this.trwIndustry.Location = new System.Drawing.Point(288, 19);
            this.trwIndustry.Name = "trwIndustry";
            this.trwIndustry.Size = new System.Drawing.Size(279, 617);
            this.trwIndustry.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(285, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Отрасль компании";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Профессиональная область";
            // 
            // trwSpecialization
            // 
            this.trwSpecialization.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.trwSpecialization.CheckBoxes = true;
            this.trwSpecialization.Location = new System.Drawing.Point(3, 19);
            this.trwSpecialization.Name = "trwSpecialization";
            this.trwSpecialization.Size = new System.Drawing.Size(279, 617);
            this.trwSpecialization.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Controls.Add(this.btnSearch);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(858, 675);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Поиск";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtGetReq);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(858, 675);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "GET запрос";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtGetReq
            // 
            this.txtGetReq.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGetReq.Location = new System.Drawing.Point(4, 4);
            this.txtGetReq.Multiline = true;
            this.txtGetReq.Name = "txtGetReq";
            this.txtGetReq.ReadOnly = true;
            this.txtGetReq.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtGetReq.Size = new System.Drawing.Size(848, 665);
            this.txtGetReq.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtSelectedFilters);
            this.groupBox1.Location = new System.Drawing.Point(6, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(730, 168);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Выбранные элементы справочника";
            // 
            // txtSelectedFilters
            // 
            this.txtSelectedFilters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSelectedFilters.Location = new System.Drawing.Point(6, 19);
            this.txtSelectedFilters.Multiline = true;
            this.txtSelectedFilters.Name = "txtSelectedFilters";
            this.txtSelectedFilters.ReadOnly = true;
            this.txtSelectedFilters.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSelectedFilters.Size = new System.Drawing.Size(718, 143);
            this.txtSelectedFilters.TabIndex = 0;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(742, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(109, 168);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Запустить поиск по выбранным параметрам";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btn2End);
            this.groupBox2.Controls.Add(this.btnNext);
            this.groupBox2.Controls.Add(this.btnPrev);
            this.groupBox2.Controls.Add(this.btn2Begin);
            this.groupBox2.Controls.Add(this.trwSearchResult);
            this.groupBox2.Location = new System.Drawing.Point(6, 177);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(845, 493);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Результат поиска";
            // 
            // trwSearchResult
            // 
            this.trwSearchResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trwSearchResult.Location = new System.Drawing.Point(6, 19);
            this.trwSearchResult.Name = "trwSearchResult";
            this.trwSearchResult.Size = new System.Drawing.Size(769, 468);
            this.trwSearchResult.TabIndex = 0;
            this.trwSearchResult.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.trwSearchResult_NodeMouseDoubleClick);
            // 
            // btn2Begin
            // 
            this.btn2Begin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn2Begin.Location = new System.Drawing.Point(781, 19);
            this.btn2Begin.Name = "btn2Begin";
            this.btn2Begin.Size = new System.Drawing.Size(58, 23);
            this.btn2Begin.TabIndex = 1;
            this.btn2Begin.Text = "<<";
            this.btn2Begin.UseVisualStyleBackColor = true;
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.Location = new System.Drawing.Point(781, 48);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(58, 23);
            this.btnPrev.TabIndex = 2;
            this.btnPrev.Text = "<";
            this.btnPrev.UseVisualStyleBackColor = true;
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(781, 77);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(58, 23);
            this.btnNext.TabIndex = 3;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            // 
            // btn2End
            // 
            this.btn2End.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn2End.Location = new System.Drawing.Point(781, 106);
            this.btn2End.Name = "btn2End";
            this.btn2End.Size = new System.Drawing.Size(58, 23);
            this.btn2End.TabIndex = 4;
            this.btn2End.Text = ">>";
            this.btn2End.UseVisualStyleBackColor = true;
            // 
            // frmHHstat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 709);
            this.Controls.Add(this.tabMain);
            this.Name = "frmHHstat";
            this.Text = "Выборки с сайта HeadHunter.ru";
            this.tabMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TreeView trwIndustry;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TreeView trwSpecialization;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtGetReq;
        private System.Windows.Forms.TreeView trwRegion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnRegionRefresh;
        private System.Windows.Forms.Button btnIndustRefresh;
        private System.Windows.Forms.Button btnSpecRefresh;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtSelectedFilters;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TreeView trwSearchResult;
        private System.Windows.Forms.Button btn2End;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btn2Begin;
    }
}

