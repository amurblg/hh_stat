﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

namespace HH_stat
{
    class HHApi
    {
        private const string _BASEURL = @"https://api.hh.ru/";
        private const string _USERAGENT = "My test app HH_stat";

        public int errCode;
        public string errMsg;

        public int VacanciesFound { get; set; }
        public int VacanciesPerPage { get; set; }
        public int VacanciesCurrentPage { get; set; }
        public int VacanciesPagesTotal { get; set; }

        public HHApi()
        {
            errCode = 0;
            errMsg = string.Empty;
        }

        private async Task<string> _GetDictionaryRawData(string dictURL)//Получение справочника в виде структуры JSON с сайта hh
        {
            string result = string.Empty;

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(_BASEURL + dictURL.ToString());
            wr.UserAgent = _USERAGENT;

            Stream objStream = null;
            StreamReader sr = null;

            try
            {
                WebResponse wresp = await wr.GetResponseAsync();
                objStream = wresp.GetResponseStream();

                sr = new StreamReader(objStream);
                result = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                this.errCode = 1;
                this.errMsg = ex.Message;
            }
            finally
            {
                if (objStream != null) objStream.Close();
                if (sr != null) sr.Close();
            }

            return result.Trim();
        }

        public async Task<List<TreeNode>> ConvertDictJsonToTreeNodeListAsync(string dictURL)//Конвертация массива JSON в список узлов Treeview
        {
            List<TreeNode> nodesList = new List<TreeNode>();
            var jsonRawData = await _GetDictionaryRawData(dictURL);//Получаем ответ от HH - справочник в виде JSON структуры

            try
            {
                JArray ja = JArray.Parse(jsonRawData);

                foreach (JToken jo in ja)
                {
                    if (jo.Type == JTokenType.Object)
                    {//Значащие поля в справочниках - только наименование (name) и id (используются для поиска)
                        TreeNode tn = new TreeNode();
                        tn.Text = ((JObject)jo).Property("name").Value.ToString().Trim();
                        tn.Name = ((JObject)jo).Property("id").Value.ToString().Trim();

                        if (jo.HasValues)
                        {
                            foreach (var item in jo.Children())
                            {
                                TreeNode newChild = _DictJObjectRecursive(item, tn);
                                if (newChild.Text.Length > 0) tn.Nodes.Add(newChild);
                            }
                        }

                        if (tn.Text.Length > 0) nodesList.Add(tn);
                    }
                }
            }
            catch (Exception ex)
            {
                this.errCode = 2;
                this.errMsg = ex.Message;
            }

            return nodesList;
        }

        private TreeNode _DictJObjectRecursive(JToken item, TreeNode trueParent)//Рекурсивная функция обхода JSON-структуры справочников
        {
            TreeNode tn = new TreeNode();
            if (item.Type == JTokenType.Object)
            {//Значащие поля в справочниках - только наименование (name) и id (используются для поиска)
                tn.Text = ((JObject)item).Property("name").Value.ToString().Trim();
                tn.Name = ((JObject)item).Property("id").Value.ToString().Trim();
            }

            if (item.HasValues)
            {
                foreach (var child in item.Children())
                {
                    TreeNode newChild = _DictJObjectRecursive(child, tn.Text.Length > 0 ? tn : trueParent);
                    if (newChild.Text.Length > 0) (tn.Text.Length > 0 ? tn : trueParent).Nodes.Add(newChild);
                }
            }

            return tn;
        }

        public async Task<string> Search(string searchParams)//Асинхронный запрос на поиск вакансий
        {
            string result = string.Empty;
            HttpWebRequest wreq = (HttpWebRequest)WebRequest.Create(_BASEURL + "vacancies?" + searchParams);
            wreq.UserAgent = _USERAGENT;

            Stream objStream = null;
            StreamReader sr = null;

            try
            {
                WebResponse wresp = await wreq.GetResponseAsync();
                objStream = wresp.GetResponseStream();

                sr = new StreamReader(objStream);
                result = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                this.errCode = 3;
                this.errMsg = ex.Message;
            }
            finally
            {
                if (objStream != null) objStream.Close();
                if (sr != null) sr.Close();
            }

            return result;
        }

        public string CreateSearchString(string paramName, Dictionary<string, string> paramKeyValuePairs)//Формирование подстроки запроса (фильтр по тем или иным справочникам)
        {
            string result = string.Empty;
            foreach (var item in paramKeyValuePairs)
            {
                result += paramName + "=" + item.Key + "&";
            }

            if (result.Trim().Length > 0) result = result.Substring(0, result.Trim().Length - 1);
            return result;
        }

        public List<TreeNode> ConvertVacancySearchResult2Treeview(string searchResult)//Парсинг объекта JSON с результатами поиска вакансий, получение списка узлов для Treeview
        {
            List<TreeNode> nodesList = new List<TreeNode>();

            try
            {
                JToken rootToken = JToken.Parse(searchResult);
                //Сначала получаем заголовочные поля результатов поиска
                this.VacanciesFound = Convert.ToInt32(((JValue)rootToken["found"]).Value);
                this.VacanciesPagesTotal = Convert.ToInt32(((JValue)rootToken["pages"]).Value);
                this.VacanciesPerPage = Convert.ToInt32(((JValue)rootToken["per_page"]).Value);
                this.VacanciesCurrentPage = Convert.ToInt32(((JValue)rootToken["page"]).Value);

                if (VacanciesFound > 0)//Перебираем массив items собственно результатов поиска - в нем хранится максимум 20 объектов вакансий
                {
                    foreach (JObject currentVacancy in rootToken["items"])
                    {
                        TreeNode tn = new TreeNode();
                        //В имени узла будет лишь наименование вакансии и фирма, ее разместившая. В свойстве Name - урл этой вакансии на сайте hh
                        tn.Text = ((JValue)currentVacancy["name"]).Value.ToString() + " (\"" + ((JValue)currentVacancy["employer"]["name"]).Value.ToString() + "\")";
                        tn.Name = ((JValue)currentVacancy["alternate_url"]).Value.ToString();

                        nodesList.Add(tn);
                    }
                }
            }
            catch (Exception ex)
            {
                this.errCode = 3;
                this.errMsg = ex.Message;
            }

            return nodesList;
        }
    }
}
